/**
*
* @brief	LED指示灯
* @author	yun
* @data		2016-12-27
* @desc		LED指示灯驱动
* @version	v0.1
*
*/

#ifndef		__PLATFORM_LED_H
#define		__PLATFORM_LED_H

typedef enum {
	PLATFORM_LED_ERR = 0,
	PLATFORM_LED_NETWORK = 1,
}platform_led_t;


int platform_led_init(void);

// 开灯
int platform_led_on(platform_led_t led);

// 关灯
int platform_led_off(platform_led_t led);

#endif

