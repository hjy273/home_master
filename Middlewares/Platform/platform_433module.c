/**
* 
* @brief	433模块驱动
* @author	yun
* @date		2016-12-22 
* @desc		433模块，控制开关和室内灯
* @version	V0.1
*
*/

#include	"platform_433module.h"

#include	"stm32f2xx.h"
#include	"platform_logger.h"
#include	<string.h>

static module433_callback_fn module433_callback = NULL;

// 消息监听回掉函数
int platform_module433_notification(module433_callback_fn callback){
	 module433_callback = callback;
}

static int rev_flag = 0;

#define		UART_RX_BUF_SIZE		32

unsigned char module_rev_buffer[UART_RX_BUF_SIZE];

//
int platform_module433_test(void){
	log_info_printf("433module: test");
	
	platform_module433_init();
	
	//char *cmd = "=999999999,R1#";
	
	//platform_module433_send((unsigned char *)cmd, strlen(cmd));
	
	while(1){
		if(rev_flag){
			log_info_printf("433module: %s", module_rev_buffer);
			rev_flag = 0;
		}
	}
	
	return 0;
}


#include 	"stm32f2xx.h"

#define     MODULE433_CLK          RCC_APB2Periph_USART1
#define     MODULE433_CLK_INIT     RCC_APB2PeriphClockCmd

#define     MODULE433_RX_PIN         	GPIO_Pin_10
#define     MODULE433_RX_SOURCE      	GPIO_PinSource10
#define     MODULE433_RX_GPIO_PORT   	GPIOA
#define     MODULE433_RX_GPIO_CLK    	RCC_AHB1Periph_GPIOA
#define     MODULE433_RX_AF          	GPIO_AF_USART1

#define     MODULE433_TX_PIN         	GPIO_Pin_9
#define     MODULE433_TX_SOURCE      	GPIO_PinSource9
#define     MODULE433_TX_GPIO_PORT   	GPIOA
#define     MODULE433_TX_GPIO_CLK    	RCC_AHB1Periph_GPIOA
#define     MODULE433_TX_AF          	GPIO_AF_USART1

#define     MODULE433_IRQn             	USART1_IRQn
#define     MODULE433	                USART1
//#define     MODULE433_IRQHandler       	USART6_IRQHandler
#define     MODULE433_IRQHandler       	USART1_IRQHandler

#define 	MODULE433_DR_Base        		((uint32_t)USART1 + 0x04)

#define     DMA_CLK_INIT                	RCC_AHB1Periph_DMA2
#define     MODULE433_RX_DMA_Stream       	DMA2_Stream2
#define     MODULE433_RX_DMA_Stream_IRQn  	DMA2_Stream2_IRQn
#define     MODULE433_RX_DMA_HTIF         	DMA_FLAG_HTIF2
#define     MODULE433_RX_DMA_TCIF         	DMA_FLAG_TCIF2

//serial_rev_typedef serial_rev_str;

int platform_module433_init(void){
    GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	//串口空闲中断
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	NVIC_InitStructure.NVIC_IRQChannel = MODULE433_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x2F;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	//串口IO口
	RCC_AHB1PeriphClockCmd(MODULE433_RX_GPIO_CLK, ENABLE);
	MODULE433_CLK_INIT(MODULE433_CLK, ENABLE);
	
	GPIO_PinAFConfig(MODULE433_TX_GPIO_PORT, MODULE433_TX_SOURCE, MODULE433_TX_AF);
	GPIO_PinAFConfig(MODULE433_RX_GPIO_PORT, MODULE433_RX_SOURCE, MODULE433_RX_AF);
	//
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = MODULE433_TX_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(MODULE433_TX_GPIO_PORT, &GPIO_InitStructure);
	//
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Pin = MODULE433_RX_PIN;
	GPIO_Init(MODULE433_RX_GPIO_PORT, &GPIO_InitStructure);
	USART_DeInit(MODULE433);
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1 ;           
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(MODULE433, &USART_InitStructure);
	
	//串口DMA接收
	DMA_InitTypeDef DMA_InitStructure;
	RCC_AHB1PeriphClockCmd(DMA_CLK_INIT, ENABLE);
	DMA_ClearFlag(MODULE433_RX_DMA_Stream, MODULE433_RX_DMA_HTIF | MODULE433_RX_DMA_TCIF);
	//
	DMA_Cmd(MODULE433_RX_DMA_Stream, DISABLE);
	DMA_DeInit(MODULE433_RX_DMA_Stream);
	DMA_InitStructure.DMA_PeripheralBaseAddr = MODULE433_DR_Base;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;         
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_InitStructure.DMA_Channel = DMA_Channel_4;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)module_rev_buffer;
	DMA_InitStructure.DMA_BufferSize = (uint16_t)UART_RX_BUF_SIZE;
	DMA_Init(MODULE433_RX_DMA_Stream, &DMA_InitStructure);
	USART_DMACmd(MODULE433, USART_DMAReq_Rx, ENABLE);
	DMA_Cmd(MODULE433_RX_DMA_Stream, ENABLE);
	//串口接收

	USART_ITConfig(MODULE433, USART_IT_IDLE, ENABLE);
	USART_Cmd(MODULE433, ENABLE);
	return 0;
}

#include	"app_conf.h"

//串口空闲代表接收完成
void MODULE433_IRQHandler(void){
	if(USART_GetITStatus(MODULE433, USART_IT_IDLE) != RESET){
		short int i = MODULE433->SR;
		i = MODULE433->DR;
		USART_ClearITPendingBit(MODULE433, USART_IT_IDLE);
		DMA_Cmd(MODULE433_RX_DMA_Stream, DISABLE);
		//将数据拷贝出DMA缓冲区
		int len = UART_RX_BUF_SIZE - DMA_GetCurrDataCounter(MODULE433_RX_DMA_Stream);
		module_rev_buffer[len] = '\0';
		rev_flag = 1;
		if(module433_callback != NULL){
			module433_callback((char *)module_rev_buffer, len);
		}
		DMA_Cmd(MODULE433_RX_DMA_Stream, ENABLE);
	} 
}





//串口发送
int platform_module433_send(unsigned char *buffer, int num){
	while(num--){
		while(USART_GetFlagStatus(MODULE433, USART_FLAG_TC) != SET);
		USART_SendData(MODULE433, *buffer++);
	}
	return num;
}

