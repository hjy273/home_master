# [Home Master]()
----

一款基于WiFi功能的家居设备控制盒子，基本功能:

- 433无线设备控制(开关、插座)
- 温湿度采集功能
- WIFI网络连接功能
- 集成串口调试芯片

该工程属于 [智能家居DIY系统](https://adai.design/design?project=homemaster-v2019) 的一部分，系统一共由四部分组成.
- **Home Master** 家居控制盒子,控制家里的开关、插座等设备，带有温湿度采集的功能.
- **Home Server**  由GO语言编写的服务端应用,负责管理设备、手机端、网页端的连接.
- **Wisdom** IOS手机控制终端,只编写IOS版本原生应用，Android端通过网页控制.
- **Home Panel** 家居网页控制面板,集成在home_server工程中.

----
该工程是3年前最开始设计的简陋网关，只能接入指定厂家的433设备, 已经不再更新
- 最新的V2019版采用Zigbee3.0, 能够接入飞利浦、宜家、小米等厂家的设备
- 最新的智能家居DIY网关项目地址: [**https://gitee.com/adaidesigner/homemaster**](https://gitee.com/adaidesigner/homemaster)

### [智能家居DIY设计V2019版介绍网站: https://adai.design/design](https://adai.design/design?project=homemaster-v2019)
![image](Documents/web-adai-design-explore.jpg)

----


## 工程结构

![](Documents/工程结构.jpg)

- 注释: 灰色背景表示文件夹，灰色框白色背景表示文件.
- Home Master 应用以服务为基本单元编写而成.
- 设备服务,管理设备与服务器连接的登陆、登出、心跳三个功能.
- 网络服务，管理WiFi与路由器的连接、断开、以及无线配置Airkiss三个功能
- 设备服务与网络服务是基础服务.

## 硬件结构框图与实体模型

- 关于 [ Home Master ] 的硬件设计，请参考Documents/Master-PCB文件夹.
- 原理图和PCB由 Altium Design 16 设计制作.


![](Documents/硬件结构.jpg)

## 需要完善的功能

- 开关设备远程配置、增删减功能(现在433开关设备的ID信息直接写在程序里).
- 盒子固件远程升级功能.
- 盒子定时上传系统运行的CPU占用率、内存等信息.
- 盒子设备断网之后的开关状态和温湿度数据离线存储.


## 版本迭代

- V1.0.0 基本功能正常使用的第一个版本






