/**
* 
* @brief	温湿度采集服务
* @author	yun
* @date		2016-12-28
* @desc		设备温湿度采集服务
* @version	v0.1
*
*/

#ifndef		__SERVICE_TEMP_H
#define		__SERVICE_TEMP_H


int service_temp_establish(void);

int service_temp_destroy(void);

int service_temp_query(void);

#endif


