/**
* 
* @brief	网络连接状态管理服务
* @author	yun
* @date		2016-12-29 13:09:51
* @version	0.1
* @desc		
*
**/


#ifndef		__SERVICE_NETWORK_H
#define		__SERVICE_NETWORK_H


int network_establish(void);
int network_destroy(void);


#endif


