/**
*
* @brief	APP 配置信息
* @author	yun
* @date		2016-12-20
* @desc		
* @version	0.1
*
*
*/

#ifndef		__APP_CONF_H
#define		__APP_CONF_H

#include	<stdio.h>
#include	<string.h>

#define		APP_NAME			"Wisdom"
#define		APP_VERSION			"1.2.1"

#define		APP_WIFI_NAME		"#daodao"
#define		APP_WIFI_PASSWORD	"howoldareyou"		// 怎么老是你

#define		DEVICE_NAME			"Home Master"
#define		DEVICE_ID			"WSDMP161206001L"
#define		DEVICE_KEY			"ojtBWCpcZizCd0ZPPgZjpw"
#define		DEVICE_DESC			"EMW3162(stm32f205+bcm43362)"


// 服务器域名
#define		APP_SERVER_DOMAIN					"112.74.217.62:9090"
// 服务器断开后 重连时间间隔 (2s)
#define		APP_SERVER_CONNECTED_INTERVAL   	2000
// 数据包最大长度
#define		APP_SERVER_PACKET_LEN_MAX			256
// 服务器超时断开时间
#define		APP_SERVER_TIMEOUT					15000
// 服务最大个数
#define		APP_SERVICE_CNT_MAX					10


// 服务器心跳间隔 (10s)
#define		SEV_DEV_HB_INTERVAL		5000
// 开关服务状态查询频率(60s/次)
#define		SEV_SWITCH_QUERY_INTERVAL			60000
// 温湿度服务状态上传频率(5*60s/次)
#define		SEV_TEMP_HUMD_QUERY_INTERVAL		300000


// app 状态
typedef enum {
	APP_NETWORK_DISCON = 0,			// 网络断开
	APP_NETWORK_CON,				// 网络连接
	APP_SERVER_DISCON,     			// 服务器断开
	APP_SERVER_CON,         		// 服务器连接成功
	APP_SERVER_LOGIN,				// 设备登陆成功
	APP_SERVER_LOGOUT,				// 设备登出
	APP_CMD,						// 串口指令输入
	APP_UPDATE,             		// 应用升级
	APP_ERR,                		// 运行错误
	APP_STATE_MAX,
}app_state_typedef;


extern app_state_typedef volatile app_state;	// 应用状态

// 设备状态改变通知
int app_state_notification(app_state_typedef state);


#endif


